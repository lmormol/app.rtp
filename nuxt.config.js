import colors from 'vuetify/es5/util/colors'

const projectDescription =
  'RTP is created out of love to travel by own means of transport, based on real needs and many years of experience in traveling by motorbike around Europe 🏍'
const projectTitle =
  'Travel costs under control, plan and remember your road trip'

export default {
  mode: 'spa',
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: '%s | ' + 'Road-Trip-Planner.com',
    title: 'Travel costs under control, plan and remember your road trip',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'og-title', property: 'og:title', content: projectTitle },
      { hid: 'description', name: 'description', content: projectDescription },
      {
        hid: 'og-desc',
        property: 'og:description',
        content: projectDescription,
      },
      { hid: 'og-image', property: 'og:image', content: `/og/rtp__og.jpg` },
    ],
    script: [
      {
        type: 'text/javascript',
        src: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyC0-NgUCcaq19la0SG0_Rj7g2XpyGroeq4&callback=Function.prototype&libraries=geometry,geocoding',
        async: true,
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons',
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Turret+Road:wght@500&display=swap',
      },
    ],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#000',
  },
  loadingIndicator: {
    name: 'three-bounce',
    color: 'black',
    background: 'white',
  },
  /*
   ** Global CSS
   */
  css: [{ src: '~/assets/style/main.css', lang: 'css' }],
  rules: [
    {
      test: /\.scss$/,
      use: ['vue-style-loader', 'css-loader', 'sass-loader'],
    },
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/editor.js',
    '~/plugins/filters.js',
    '~/plugins/fireinit.js',
    '~/plugins/fireauth.js',
    '~/plugins/hotjar.js',
    '~/plugins/notifier.js',
    '~/plugins/rtp-helpers.js',
    '~/plugins/vuetify.js',
    '~/plugins/vue-clipboard.js',
    '~/plugins/vue-moment.js',
    '~/plugins/vue-countdown.js',
    '~/plugins/vue-flags.js',
  ],
  /*
   ** nuxtjs.org/examples/i18n
   */
  generate: {
    routes: [
      // A - Z

      // EN
      '/',
      '/about',
      '/byebye',
      '/data-deletion',
      '/features',
      '/login',
      '/my-trips',
      '/observed-trips',
      '/plan-new',
      '/traveler',
      '/trip-edit',
      '/trip-view',
      '/user-settings',
      '/privacy-policy',

      // PL
      '/pl',
      '/pl/about',
      '/pl/byebye',
      '/pl/data-deletion',
      '/pl/features',
      '/pl/login',
      '/pl/my-trips',
      '/pl/observed-trips',
      '/pl/plan-new',
      '/pl/traveler',
      '/pl/trip-edit',
      '/pl/trip-view',
      '/pl/user-settings',
      '/pl/privacy-policy',
    ],
  },
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/google-analytics',
    '@nuxtjs/pwa',
    '@nuxtjs/composition-api/module',
    [
      'nuxt-i18n',
      {
        vueI18nLoader: true,
        lazy: true,
        langDir: 'locales/',
        locales: [
          { code: 'en', iso: 'en-US', file: 'en.json', name_key: 'english' },
          { code: 'pl', iso: 'pl-PL', file: 'pl.json', name_key: 'polish' },
        ],
        defaultLocale: 'en',
        vueI18n: {
          fallbackLocale: 'en',
        },
        detectBrowserLanguage: {
          useCookie: true,
          cookieKey: 'i18n_redirected',
          onlyOnRoot: false,
          alwaysRedirect: true,
        },
      },
    ],
  ],
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
  },
  router: {
    middleware: ['router-auth'],
  },
  vendor: ['firebase', 'vuetify'],
  manifest: {
    name: 'Road Trip Planner',
    description: 'Travel costs under control, plan and remember your road trip',
    short_name: 'RTP',
    lang: 'en',
    display: 'fullscreen',
    start_url: '/?standalone=true',
    theme_color: '#000000',
    background_color: '#ffffff',
    icons: [
      {
        src: '/icons/icon.png',
        sizes: '512x512',
        type: 'image/png',
      },
      {
        src: '/icons/maskable_icon.png',
        sizes: '1024x1024',
        type: 'image/png',
        purpose: 'maskable',
      },
    ],
  },
  googleAnalytics: {
    id: 'UA-2964660-52',
  },
  env: {
    isProduction: process.env.NODE_ENV === 'production',
  },
}
