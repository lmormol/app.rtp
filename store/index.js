import { auth, GoogleProvider, FacebookProvider } from '@/plugins/fireinit.js'

export const state = () => ({
  user: null,
  locales: ['en', 'pl'],
  locale: 'en'
})

export const mutations = {
  setUser(state, payload) {
    state.user = payload
  },
  setLang (state, locale) {
    if (state.locales.includes(locale)) {
      state.locale = locale

      // TODO: Is anywhere better place for switching lang for vue-moment
      this._vm.$moment.locale(locale)
    }
  }
}

export const actions = {
  signInWithGoogle() {
    return new Promise((resolve) => {
      // auth.signInWithRedirect(GoogleProvider)
      auth.signInWithPopup(GoogleProvider)
      resolve()
    })
  },
  signInWithFB() {
    return new Promise((resolve) => {
      auth.signInWithPopup(FacebookProvider)
      resolve()
    })
  },
  signOut({commit}) {
    auth.signOut().then(() => {
      commit('setUser', null)
    }).catch(error => console.log(error))
  }
}

export const getters = {
  activeUser: (state) => {
    return state.user
  }
}

export const strict = false

