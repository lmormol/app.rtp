export const state = () => ({
  title: '',
  fieldName: '',
  fieldHint: '',
  fieldData: {},
  updatedField: '',
})

export const mutations = {
  showTextDialog (state, payload) {
    state.title = payload.title
    state.fieldName = payload.fieldName
    state.fieldData = payload.fieldData
    state.fieldHint = payload.fieldHint
  },
  changeTextDialog (state, payload) {
    state.updatedField = payload.actionName
  },
}
