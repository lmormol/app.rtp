const mapSettings = {
  mapId: '6be66d292360489b',
  clickableIcons: false,
  streetViewControl: false,
  panControlOptions: false,
  gestureHandling: 'cooperative',
  backgroundColor: '#fff',
  mapTypeControl: false,
  zoomControlOptions: {
    style: 'SMALL',
  },
  zoom: 2,
  minZoom: 2,
  maxZoom: 12,
}

export { mapSettings }
