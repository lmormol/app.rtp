import { profileTraveler } from "./fireinit";

function updateTravelerProfile(user) {
    const profileTravelerRef = profileTraveler.doc(user.uid)
    profileTravelerRef.set({
        userId: user.uid,
        photoUrl: user.photoURL,
        displayName: user.displayName
    }, { merge: true });
};

export {
    updateTravelerProfile
}
