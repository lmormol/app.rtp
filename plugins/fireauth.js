import { auth } from '@/plugins/fireinit.js'
import { updateTravelerProfile } from '@/plugins/userProfileActions.js'

export default context => {
  const { store } = context

  return new Promise((resolve, reject) => {
    auth.onAuthStateChanged(user => {
      if (user) {
        updateTravelerProfile(user);
        return resolve(store.commit('setUser', user))
      }
      return resolve()
    })
  })
}
