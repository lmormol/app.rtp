import { DB, tripCostsCollection, tripsCollection } from './fireinit'
import firebase from 'firebase/app'
import 'firebase/firestore'
const Helper = {
  install(Vue, options) {
    Vue.prototype.$countTripLength = function (startDate, endDate) {
      let start = this.$moment(startDate)
      let end = this.$moment(endDate)
      return end.diff(start, 'days') + 1
    }

    Vue.prototype.$durationInHoursAndMinutes = function (value) {
      if (!value) return ''
      value = value * 1000
      const hours = Math.floor(this.$moment.duration(value).asHours())
      const minutes = this.$moment.duration(value).minutes()
      if (hours > 0) {
        return `${hours} h ${minutes} m`
      } else {
        return `${minutes} m`
      }
    }

    Vue.prototype.$keepFocusOnMe = function (event) {
      event.target.parentNode.focus()
    }

    Vue.prototype.$updateDaySumCostsAfterDeletingDay = function (
      tripId,
      category,
      costAmount
    ) {
      const decrement = firebase.firestore.FieldValue.increment(-costAmount)
      const costCategory = category + 'CostsSum'

      tripsCollection.doc(tripId).update({
        [costCategory]: decrement,
      })
    }

    Vue.prototype.$editTrip = function (tripId) {
      this.$router.push(`${this.localePath('trip-edit')}?id=${tripId}`)
    }

    Vue.prototype.$viewTrip = function (tripId) {
      this.$router.push(`${this.localePath('trip-view')}?id=${tripId}`)
    }

    Vue.prototype.$viewTravelerProfile = function (travelerId) {
      this.$router.push(`${this.localePath('traveler')}?id=${travelerId}`)
    }

    Vue.prototype.$openHomePage = function () {
      this.$router.push(`${this.localePath('index')}`)
    }

    Vue.prototype.$deleteAllDayCosts = function (dayId) {
      let vm = this
      tripCostsCollection
        .where('dayId', '==', dayId)
        .get()
        .then(function (querySnapshot) {
          const batch = DB.batch()
          querySnapshot.forEach(function (doc) {
            batch.delete(doc.ref)
            vm.$updateDaySumCostsAfterDeletingDay(
              doc.data().tripId,
              doc.data().category,
              doc.data().convertedAmount
            )
          })
          batch.commit()
        })
    }

    Vue.prototype.$slugAddress = function (address) {
      return address.replace(/\s+/g, '-').toLowerCase()
    }

    Vue.prototype.$countCostPerDay = function (tripLength, tripTotalCosts) {
      return this.$options.filters.itsPrice(tripTotalCosts) / tripLength
    }

    Vue.prototype.$textLengthThreshold = 100

    Vue.prototype.$tripMaxLengthInDaysLimit = 190

    Vue.prototype.$supportedCurrencies = [
      {
        currency_name_pl: 'ALL – Lek albański',
        currency_name_en: 'ALL – Albanian lek',
        currency_code: 'ALL',
      },
      {
        currency_name_pl: 'BAM – Marka zamienna Bośni i Hercegowiny',
        currency_name_en: 'BAM – Bosnia and Herzegovina convertible mark',
        currency_code: 'BAM',
      },
      {
        currency_name_pl: 'BGN – Lew bułgarski',
        currency_name_en: 'BGN – Bulgarian lev',
        currency_code: 'BGN',
      },
      {
        currency_name_pl: 'BYR – Rubel białoruski',
        currency_name_en: 'BYR – Belarusian ruble',
        currency_code: 'BYR',
      },
      {
        currency_name_pl: 'CHF – Frank szwajcarski',
        currency_name_en: 'CHF – Swiss franc',
        currency_code: 'CHF',
      },
      {
        currency_name_pl: 'CZK – Korona czeska',
        currency_name_en: 'CZK – Czech koruna',
        currency_code: 'CZK',
      },
      {
        currency_name_pl: 'DKK – Korona duńska',
        currency_name_en: 'DKK – Danish krone',
        currency_code: 'DKK',
      },
      {
        currency_name_pl: 'EUR – Euro',
        currency_name_en: 'EUR – Euro',
        currency_code: 'EUR',
      },
      {
        currency_name_pl: 'GBP – Funt brytyjski',
        currency_name_en: 'GBP – British pound',
        currency_code: 'GBP',
      },
      {
        currency_name_pl: 'HRK – Kuna chorwacka',
        currency_name_en: 'HRK – Croatian kuna',
        currency_code: 'HRK',
      },
      {
        currency_name_pl: 'HUF – Węgierski forint',
        currency_name_en: 'HUF – Hungarian forint',
        currency_code: 'HUF',
      },
      {
        currency_name_pl: 'ISK – Korona islandzka',
        currency_name_en: 'ISK – Icelandic krona',
        currency_code: 'ISK',
      },
      {
        currency_name_pl: 'MDL – Lej mołdawski',
        currency_name_en: 'MDL – Moldovan leu',
        currency_code: 'MDL',
      },
      {
        currency_name_pl: 'MKD – Denar macedoński',
        currency_name_en: 'MKD – Macedonian denar',
        currency_code: 'MKD',
      },
      {
        currency_name_pl: 'NOK – Korona norweska',
        currency_name_en: 'NOK – Norwegian krone',
        currency_code: 'NOK',
      },
      {
        currency_name_pl: 'PLN – Polski złoty',
        currency_name_en: 'PLN – Polish zloty',
        currency_code: 'PLN',
      },
      {
        currency_name_pl: 'RON – Lej rumuński',
        currency_name_en: 'RON – Romanian leu',
        currency_code: 'RON',
      },
      {
        currency_name_pl: 'RSD – Dinar serbski',
        currency_name_en: 'RSD – Serbian dinar',
        currency_code: 'RSD',
      },
      {
        currency_name_pl: 'RUB – Rubel rosyjski',
        currency_name_en: 'RUB – Russian ruble',
        currency_code: 'RUB',
      },
      {
        currency_name_pl: 'SEK – Korona szwedzka',
        currency_name_en: 'SEK – Swedish krona',
        currency_code: 'SEK',
      },
      {
        currency_name_pl: 'TRY – Lir turecki',
        currency_name_en: 'TRY – Turkish lira',
        currency_code: 'TRY',
      },
      {
        currency_name_pl: 'UAH – Hrywna ukraińska',
        currency_name_en: 'UAH – Ukrainian hryvnia',
        currency_code: 'UAH',
      },
      {
        currency_name_pl: 'USD – Dolar amerykański',
        currency_name_en: 'USD – United States dollar',
        currency_code: 'USD',
      },
    ]

    Vue.prototype.$transparentImgH =
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAACCAQAAAA3fa6RAAAADklEQVR42mNkAANGCAUAACMAA2w/AMgAAAAASUVORK5CYII='

    Vue.prototype.$transparentImgV =
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAADCAQAAAAT4xYKAAAADklEQVR42mNkAAJGOAEAAC0ABNxMS2YAAAAASUVORK5CYII='

    Vue.prototype.$toggleBodyClass = function (className) {
      document.body.classList.toggle(className)
    }

    Vue.prototype.$minTwoDigits = function (n) {
      return (n < 10 ? '0' : '') + n
    }
  },
}

export default Helper
