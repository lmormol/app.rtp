import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/database'

let config = {
  apiKey: 'AIzaSyDTajYb0QsmyLTawq5kCxdYjtoxb8RpmZ4',
  authDomain: 'road-trip-planner.com',
  projectId: 'road--trip--planner',
  storageBucket: 'road--trip--planner.appspot.com',
  messagingSenderId: '441131377676',
  appId: '1:441131377676:web:15f5397cda368f33e67a29',
  measurementId: 'G-T1WPWC2730',
}

!firebase.apps.length ? firebase.initializeApp(config) : ''
const auth = firebase.auth()
const DB = firebase.firestore()
const GoogleProvider = new firebase.auth.GoogleAuthProvider()
const FacebookProvider = new firebase.auth.FacebookAuthProvider()

/*
 * Firestore collections
 */

// P
const profileSettings = DB.collection('profileSettings')
const profileTraveler = DB.collection('profileTraveler')

// T
const tripsCollection = DB.collection('trips')
const tripDaysCollection = DB.collection('tripDays')
const tripCostsCollection = DB.collection('tripCosts')

// R
const rtpSettings = DB.collection('rtpSettings')

export {
  auth,
  DB,
  GoogleProvider,
  FacebookProvider,
  profileSettings,
  profileTraveler,
  rtpSettings,
  tripsCollection,
  tripCostsCollection,
  tripDaysCollection,
}
