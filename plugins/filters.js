import Vue from 'vue'

Vue.filter('itsPrice', val => Number(val / 100).toFixed(2));

Vue.filter('itsRoundedPrice', val => Math.round(Number(val / 100)));

Vue.filter('truncate', function (text, stop, clamp) {
    let truncatedText;
    text ? truncatedText = text : truncatedText = '';
    return truncatedText.slice(0, stop) + (stop < truncatedText.length ? clamp || '...' : '')
})
