import Vue from 'vue'
import Hotjar from 'vue-hotjar'

Vue.use (Hotjar, {
  id: '2832575',
  isProduction: process.env.isProduction
})
