export default ({ app, store }, inject) => {
  inject('editor', {
    showTextDialog ({ title = '', fieldName = '', fieldHint = '', fieldData = {} }) {
      store.commit('dialog/showTextDialog', { title, fieldName, fieldHint, fieldData })
    },
    changeTextDialog({ actionName = '' }) {
      store.commit('dialog/changeTextDialog', { actionName })
    }
  })
}
