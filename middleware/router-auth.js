// This is `@middleware/router-auth.js`

export default function({ store, redirect, route }) {

  // admin
  store.state.user == null && isAdminRoute(route) ? redirect('/') : ''

  // login
  store.state.user != null && route.name == 'login' ? redirect('/') : ''

  // trip-edit
  store.state.user == null && route.name == 'trip-edit' ? redirect('/') : ''

}

function isAdminRoute(route) {
  if (route.matched.some(record => record.path == '/admin')) {
    return true
  }
}
