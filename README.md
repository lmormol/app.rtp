# road-trip-planner.com

> The Road Trip Planner

# Prerequisites

* yarn -- https://yarnpkg.com 
* firebase CLI -- https://firebase.google.com/docs/cli

## Build Setup

``` bash
# INSTALL DEPENDENCIES
$ yarn install

# SERVE WITH HOT RELOAD AT LOCALHOST:3000
$ yarn dev

# BUILD FOR PRODUCTION ( PUBLIC DIRECTORY 'DIST' )
$ yarn build

# INSTALL FIREBASE CLI
$ curl -sL https://firebase.tools | bash

# SIGN IN TO FIREBASE
$ firebase login

# DEPLOY TO PRODUCTION 
$ yarn fdeploy

```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
